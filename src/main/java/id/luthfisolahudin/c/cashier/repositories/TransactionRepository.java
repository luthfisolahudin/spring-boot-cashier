package id.luthfisolahudin.c.cashier.repositories;

import id.luthfisolahudin.c.cashier.models.Transaction;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

}
