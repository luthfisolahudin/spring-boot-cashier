package id.luthfisolahudin.c.cashier.repositories;

import id.luthfisolahudin.c.cashier.models.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {

}
