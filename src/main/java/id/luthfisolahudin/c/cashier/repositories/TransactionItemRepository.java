package id.luthfisolahudin.c.cashier.repositories;

import id.luthfisolahudin.c.cashier.models.TransactionItem;
import org.springframework.data.repository.CrudRepository;

public interface TransactionItemRepository extends CrudRepository<TransactionItem, Long> {

}
