package id.luthfisolahudin.c.cashier.services;

import id.luthfisolahudin.c.cashier.dto.TransactionItemQuantity;
import id.luthfisolahudin.c.cashier.models.Transaction;
import id.luthfisolahudin.c.cashier.models.TransactionItem;
import id.luthfisolahudin.c.cashier.repositories.TransactionItemRepository;
import id.luthfisolahudin.c.cashier.repositories.TransactionRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.stream.StreamSupport;

@Component
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionItemRepository transactionItemRepository;

    public Transaction createTransaction(Iterable<TransactionItemQuantity> items) {
        val transaction = newTransaction();

        transactionItemRepository.saveAll(
            StreamSupport
                .stream(items.spliterator(), true)
                .map(item -> new TransactionItem(transaction, item.getItem(), item.getQty()))
                .toList()
        );

        return transaction;
    }

    protected Transaction newTransaction() {
        return transactionRepository.save(new Transaction(Instant.now()));
    }

}
