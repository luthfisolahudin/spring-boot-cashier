package id.luthfisolahudin.c.cashier.dto;

import id.luthfisolahudin.c.cashier.models.Item;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class TransactionItemQuantity {

    @NonNull
    private Item item;

    @NonNull
    private Integer qty;

}
