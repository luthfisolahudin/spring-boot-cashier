package id.luthfisolahudin.c.cashier.http.requests;

import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class CreateTransactionRequest {

    @NonNull
    private Collection<Item> items = new ArrayList<>();

    @Data
    public static class Item {

        @NonNull
        private Long itemId;

        @NonNull
        private Integer qty;

    }

}
