package id.luthfisolahudin.c.cashier.http.controllers;

import id.luthfisolahudin.c.cashier.dto.TransactionItemQuantity;
import id.luthfisolahudin.c.cashier.exceptions.ItemNotFound;
import id.luthfisolahudin.c.cashier.http.requests.CreateTransactionRequest;
import id.luthfisolahudin.c.cashier.repositories.ItemRepository;
import id.luthfisolahudin.c.cashier.repositories.TransactionRepository;
import id.luthfisolahudin.c.cashier.services.TransactionService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("items", itemRepository.findAll());

        return "home/index";
    }

    @GetMapping("/transactions")
    public String listTransaction(Model model) {
        model.addAttribute("transactions", transactionRepository.findAll());

        return "home/transaction";
    }

    @PostMapping("/transactions")
    @ResponseBody
    public Boolean createTransaction(@RequestBody CreateTransactionRequest request) {
        val items = request.getItems()
            .parallelStream()
            .filter(requestItem -> requestItem.getItemId() > 0)
            .map(requestItem -> {
                val item = itemRepository.findById(requestItem.getItemId()).orElseThrow(ItemNotFound::new);

                return TransactionItemQuantity
                    .builder()
                    .item(item)
                    .qty(requestItem.getQty())
                    .build();
            })
            .toList();

        transactionService.createTransaction(items);

        return true;
    }

}
