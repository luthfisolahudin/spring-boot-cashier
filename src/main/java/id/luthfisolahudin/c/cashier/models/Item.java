package id.luthfisolahudin.c.cashier.models;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "items")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private Long id = null;

    @Column(nullable = false)
    @NonNull
    private String name;

    @Column(nullable = false)
    @NonNull
    private Double price = 0d;

    @OneToMany(
        targetEntity = TransactionItem.class,
        mappedBy = "item",
        fetch = FetchType.LAZY
    )
    private Collection<TransactionItem> transactionItems = new ArrayList<>();

    public Item(String name, Double price) {
        this.name = name;
        this.price = price;
    }

}
