package id.luthfisolahudin.c.cashier.models;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "transactions")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private Long id = null;

    @Column(nullable = false)
    @NonNull
    private Instant time;

    @OneToMany(
        targetEntity = TransactionItem.class,
        mappedBy = "transaction",
        fetch = FetchType.LAZY
    )
    private Collection<TransactionItem> transactionItems = new ArrayList<>();

    public Transaction(Instant time) {
        this.time = time;
    }

}
