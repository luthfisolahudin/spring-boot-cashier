package id.luthfisolahudin.c.cashier.models;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Table(name = "transaction_items")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TransactionItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private Long id = null;

    @JoinColumn(
        name = "transaction_id",
        columnDefinition = "BIGINT UNSIGNED",
        nullable = false
    )
    @ManyToOne(
        targetEntity = Transaction.class,
        fetch = FetchType.LAZY,
        optional = false
    )
    @NonNull
    private Transaction transaction;

    @JoinColumn(
        name = "item_id",
        columnDefinition = "BIGINT UNSIGNED",
        nullable = false
    )
    @ManyToOne(
        targetEntity = Item.class,
        fetch = FetchType.LAZY,
        optional = false
    )
    @NonNull
    private Item item;

    @Column(nullable = false)
    @NonNull
    private Integer qty = 1;

    public TransactionItem(Transaction transaction, Item item, Integer qty) {
        this.transaction = transaction;
        this.item = item;
        this.qty = qty;
    }

}
