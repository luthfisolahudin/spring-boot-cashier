{ sources ? import ./nix/sources.nix }:

let
  java_version = "21";

  pkgs = import sources.nixpkgs { overlays = []; config = {}; };

  java = pkgs."temurin-bin-${java_version}";
  niv = (import sources.niv {}).niv;
in

pkgs.mkShell {
  buildInputs = [
    java
    niv
  ];
}
